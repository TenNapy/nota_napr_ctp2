function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Loads units from specified area",
		parameterDefs = {
			{ 
				name = "unitIDsToSend",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "positions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

local TAB = "    "


function Run(self, unitIds, p)
	
	
	
	if not self.initialized then
		
		if (#p.positions == 0) then
			Spring.Echo("no positions  SUCCESS")
			return SUCCESS
		end
		
		if(#p.positions > #p.unitIDsToSend) then 
			Spring.Echo("Too many psoitions not enough units")
			return FAILURE
		end
		unitAndPos = {}
		
		for i = 1, #p.positions do
			
			currPos = p.positions[i]
			Spring.Echo("Gonna give comm", currPos)
			if currPos ~= nil then
				Spring.GiveOrderToUnit(p.unitIDsToSend[i], CMD.MOVE, {currPos.x, currPos.y, currPos.z},{})
				Spring.Echo("Command given")
				unitAndPos[p.unitIDsToSend[i]] = currPos
			end
		end
		self.initialized = true
	end
	

	
	
	
	
	
	
	
	arrivals = {}
	for id, pos in pairs(unitAndPos) do 
		if not Spring.ValidUnitID(id) then
			Spring.Echo("Some of the units died")
			return FAILURE
		end
		
		cuurUnitArrived = Arrived(id, pos, 10)
		if not cuurUnitArrived then
			return RUNNING
		end
		
		arrivals[#arrivals + 1] = Arrived(id,pos,10)
		
	end
	
	allReached = false
	for i = 1, #arrivals do
		if(arrivals[i]) then
			allReached = true
		else
			allReached = false
			break
		end
	end
	if(allReached) then
		Spring.Echo("Positions reached")
		return SUCCESS
	else
		return RUNNING
	end
	
	
end

function Reset(self)
	self.initialized = false
end

function Arrived(unit, vecB, approx)
	unitPos = {Spring.GetUnitPosition(unit)}
	
	-- we do not care about Y value
	Spring.Echo(unitPos.x)
	if(math.abs(unitPos[1] - vecB.x) <= approx and math.abs(unitPos[3] - vecB.z) <= approx) then
		return true
	else
		return false
	end
end