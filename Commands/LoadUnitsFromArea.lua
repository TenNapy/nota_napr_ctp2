function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Loads units from specified area",
		parameterDefs = {
			{ 
				name = "transporterID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "loadingArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{centerX = 0,centerY = 0, centerZ = 0, radius = 0}",
			},
			{ 
				name = "primaryUnitIDsToload",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

local TAB = "    "


-- Success if primaryUnitIDsToload are loaded in transporterID
-- Running 
-- Failure

function Run(self, unitIds, p)
	
	if not self.initialized then
		
		if(#p.primaryUnitIDsToload == 0) then 
			return SUCCESS
		end

		
		unitsInRange = Spring.GetUnitsInSphere(p.loadingArea.centerX, p.loadingArea.centerY,p.loadingArea.centerZ,p.loadingArea.radius)
		if(#unitsInRange >= #p.primaryUnitIDsToload) then
			for i = 1, #p.primaryUnitIDsToload do
				isInRange = false
				for j = 1, #unitsInRange do
					if(unitsInRange[j] == p.primaryUnitIDsToload[i]) then
						isInRange = true
					end
				end
				
				if not isInRange then
					Spring.Echo("Units are not in loading area")
					return FAILURE
				end
			end
		else
			Spring.Echo("number of units in area",#unitsInRange, "is smaller than number of units to load",primaryUnitIDsToload)
			return FAILURE
		end
		Spring.GiveOrderToUnit(p.transporterID, CMD.LOAD_UNITS, {p.loadingArea.centerX, p.loadingArea.centerY,p.loadingArea.centerZ,p.loadingArea.radius}, {})
		self.initialized = true
	end
	
	loadedUnits = Spring.GetUnitIsTransporting(p.transporterID)
	Spring.Echo(#loadedUnits, #p.primaryUnitIDsToload)
	if (#loadedUnits == #p.primaryUnitIDsToload) then
		if(Sensors.nota_napr_ctp2.IsLoadedWithUnits(loadedUnits, p.primaryUnitIDsToload)) then
			return SUCCESS
		end
	end
	
	
	
	for i = 1, #p.primaryUnitIDsToload do
		if not(Spring.ValidUnitID(p.primaryUnitIDsToload[i])) then
			Spring.Echo("not valid")
			return FAILURE
		end
	end
	
	return RUNNING
	
	
	
end

function Reset(self)
	self.initialized = false
end
