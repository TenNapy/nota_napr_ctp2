function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Loads units from specified area",
		parameterDefs = {
			{ 
				name = "transporterID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unLoadingArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{centerX = 0,centerY = 0, centerZ = 0, radius = 0}",
			}
		}
	}
end

local TAB = "    "


-- Success if primaryUnitIDsToload are loaded in transporterID
-- Running 
-- Failure

function Run(self, unitIds, p)
	
	if not self.initialized then
		
		Spring.GiveOrderToUnit(p.transporterID, CMD.UNLOAD_UNITS, {p.unLoadingArea.centerX, p.unLoadingArea.centerY,p.unLoadingArea.centerZ,p.unLoadingArea.radius}, {})
		self.initialized = true
	end
	
	loadedUnits = Spring.GetUnitIsTransporting(p.transporterID)

	if (#loadedUnits == 0) then
			return SUCCESS
	end
	
	
	
	
	
		if not(Spring.ValidUnitID(p.transporterID)) then
			Spring.Echo("not valid transproter")
			return FAILURE
		end
	
	return RUNNING
	
	
	
end

function Reset(self)
	self.initialized = false
end
