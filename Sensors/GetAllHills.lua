local sensorInfo = {
	name = "GetAllHills.lua",
	desc = "Makes a grid over ",
	author = "Dopravnik",
	date = "2020-05-13",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns a list of all enemy IDs in rectangle around a chosen unit
return function(requiredHeight, pointDetectionsSpacing)
	mapSizeX = Game.mapSizeX
	mapSizeZ = Game.mapSizeZ
	currentX = pointDetectionsSpacing/2
	currentZ = pointDetectionsSpacing/2
	
	hills = {}
	
	while currentX < mapSizeX do
		height = Spring.GetGroundHeight(currentX, currentZ)
		if(height == requiredHeight) then
			hills[#hills + 1] = Vec3(currentX,height,currentZ)
		end
		
		currentX = currentX + pointDetectionsSpacing		
		if(currentX > mapSizeX) then
			currentX = pointDetectionsSpacing/2
			currentZ = currentZ + pointDetectionsSpacing
			if(currentZ > mapSizeZ) then	
				
			--	return hills
			-- does not work - should remove duplicates
				return hills
				--
			end
		end	
	end
	Spring.Echo("Could not finish checking for hills stuck at ", currentX, currentZ)
	return nil
end
