local sensorInfo = {
	name = "GetAllHills.lua",
	desc = "Makes a grid over ",
	author = "Dopravnik",
	date = "2020-05-13",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns a list of all enemy IDs in rectangle around a chosen unit
return function(parent, toRemove)
	
	sorted = {}
	for i =1, #parent do
		removeThis = false
		for j =1, #toRemove do
			if(parent[i] == toRemove [j]) then
				removeThis = true
			end
		end
		if(removeThis == false) then
			sorted[#sorted +1 ] = parent[i]
		end
	end
	
 return sorted
	
	 
end