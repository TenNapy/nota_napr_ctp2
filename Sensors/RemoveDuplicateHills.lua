local sensorInfo = {
	name = "GetAllHills.lua",
	desc = "Makes a grid over ",
	author = "Dopravnik",
	date = "2020-05-13",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns a list of all enemy IDs in rectangle around a chosen unit
return function(oldHills, pointDetectionsSpacing)
		 madeChanges = false
			 
			--	return hills
			-- does not work - should remove duplicates
			Spring.Echo(#oldHills)
			for i = 1, #oldHills do
				for j = 1, #oldHills do
					point  = oldHills[i]
				
					otherPoint = oldHills[j]
					if(point ~= nil and otherPoint ~=nil) then
						if not(( point.x == otherPoint.x) and ( point.z == otherPoint.z)) then
							xDiff = math.abs(point.x - otherPoint.x)
							zDiff = math.abs(point.z - otherPoint.z)
							if ((xDiff == pointDetectionsSpacing and zDiff == 0) or (xDiff == 0 and zDiff == pointDetectionsSpacing) or (xDiff == pointDetectionsSpacing and zDiff == pointDetectionsSpacing) or ( xDiff == 0 and zDiff==0)) then
								oldHills[i] = nil
								madeChanges = true
								Spring.Echo("Removing", oldHills[i])
							end
						end
					end
				end
			end	
		
		betterHills = {}
		for index, val in pairs(oldHills) do
			betterHills[#betterHills +1 ] = val
		end
		
		
		
		
		if madeChanges then
			Spring.Echo("Again")
			Sensors.nota_napr_ctp2.RemoveDuplicateHills(betterHills, pointDetectionsSpacing)


		else
			Spring.Echo("finished",#betterHills, betterHills[1],  betterHills[2], betterHills[3], betterHills[4])
			return betterHills
		end
end
