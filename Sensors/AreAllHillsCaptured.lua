local sensorInfo = {
	name = "GetAllHills.lua",
	desc = "Makes a grid over ",
	author = "Dopravnik",
	date = "2020-05-13",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns a list of all enemy IDs in rectangle around a chosen unit
return function(missionInfo)
	
	hillsCaptured = missionInfo.areasOccupied
	for i = 1, #hillsCaptured do
		if(hillsCaptured[i] == false) then
			return false
		end
	end
	return true
end